/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorio1nimer2021;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author Nimer
 */
public class Laboratorio1nimer2021 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        crearMatriz();
        menul();
    }
    public static String[][] matriz = new String[7][4]; ///tabla de posicion.
    public static Scanner numero = new Scanner(System.in);
    public static Scanner letra = new Scanner(System.in);

    public static ArrayList<String> partido = new ArrayList<>();//lista para guardar equipo enfrentados

    public static ArrayList<String> equiposganados = new ArrayList<>();//guarda los partidos que gana cada equipo
    public static ArrayList<Integer> partidosganados = new ArrayList<>();

    public static void crearMatriz() {
        String mostrar = "";
        for (int x = 0; x < matriz.length; x++) {
            for (int y = 0; y < matriz[x].length; y++) {
                //AGREGA SOLO LA PARTE DE ARRIBA
                matriz[x][y] = "0";
                matriz[0][0] = "Nombre     ";
                matriz[0][1] = "GF";
                matriz[0][2] = "GC";
                matriz[0][3] = "PTS";
                //AGREGA EQUIPOS
                matriz[1][0] = "LDA        ";
                matriz[2][0] = "SAPRISA    ";
                matriz[3][0] = "SAN CARLOS ";
                matriz[4][0] = "HEREDIA    ";
                matriz[5][0] = "CARTAGINES ";
                matriz[6][0] = "PUNTARENAS ";
                mostrar = mostrar + "|" + matriz[x][y];
            }//fin del ford columna
            mostrar = mostrar + "\n";
        }//fin del ford fila
        //System.out.println(mostrar);
    }

    public static void menul() {
        boolean sigue = true;
        while (sigue == true) {
            int men = 0;
            System.out.println("/////MENU/////");
            System.out.println("1=Jugar:");
            System.out.println("2=Tabla de posiciones:");
            System.out.println("3=Reporte:");
            System.out.println("4=Salir:");
            System.out.println("//////////////");
            men = numero.nextInt();
            if (men == 1) {//MENU 1
                emparejar();
            }//fin del if 1
            if (men == 2) {//MENU 2                
                tabla();
            }//fin del if 2
            if (men == 3) { //MENU 3
                reporte();
            }//fin del if 3
            if (men == 4) { //SALIR
                sigue = false;
                break;
            }//fin if 4
        }//fin while

    }

    public static void emparejar() {
        //ArrayList<String> partido = new ArrayList<>();
        partido.clear();
        String equipo1, equipo2, equipo3, equipo4, equipo5, equipo6 = "";

        int ranequpo1, ranequpo2 = 0;
        //partido
        Random random = new Random();//random para sacar equipos

        boolean entra = true;
        int variableveri = 0;
        while (entra == true) {  //while para agregar los 2 partidos     
            ranequpo1 = random.nextInt(6);//saca equipo 1
            ranequpo2 = random.nextInt(6);
            if (ranequpo1 == ranequpo2) {

            }

            if (ranequpo1 != ranequpo2) {
                String valeq2 = "";//,valeq3,valeq4,valeq5,valeq6="";
                String valeq1 = "";
                ///dar valor a los equipos 1
                if (ranequpo1 == 0) {
                    valeq1 = "LDA        ";
                }
                if (ranequpo1 == 1) {
                    valeq1 = "SAPRISA    ";
                }
                if (ranequpo1 == 2) {
                    valeq1 = "SAN CARLOS ";
                }
                if (ranequpo1 == 3) {
                    valeq1 = "HEREDIA    ";
                }
                if (ranequpo1 == 4) {
                    valeq1 = "CARTAGINES ";
                }
                if (ranequpo1 == 5) {
                    valeq1 = "PUNTARENAS ";
                }
                ///////////////
                if (ranequpo2 == 0) {
                    valeq2 = "LDA        ";
                }
                if (ranequpo2 == 1) {
                    valeq2 = "SAPRISA    ";
                }
                if (ranequpo2 == 2) {
                    valeq2 = "SAN CARLOS ";
                }
                if (ranequpo2 == 3) {
                    valeq2 = "HEREDIA    ";
                }
                if (ranequpo2 == 4) {
                    valeq2 = "CARTAGINES ";
                }
                if (ranequpo2 == 5) {
                    valeq2 = "PUNTARENAS ";
                }
                boolean verifalso = true;
                boolean verireal = false;

                for (int x = 0; x < partido.size(); x++) {
                    if (valeq1.equals(partido.get(x))) {
                        verifalso = false;
                        break;
                    }///
                    if (valeq2.equals(partido.get(x))) {
                        verifalso = false;
                        break;
                    }
                    verireal = true;

                }//fin de for

                if (verifalso == true) {
                    variableveri = variableveri + 1;
                    partido.add(valeq1);
                    partido.add(valeq2);
                }

            }//fin del if
            if (variableveri == 3) {
                entra = false;
                break;
            }

            /////////////////////////////////////////////////////////////////////// 
        } //fin del while      
        System.out.println(partido);
        //menul();
        partido();
    }//fin de la funcion jugar

    public static void partido() {
        Random random = new Random();
        String pequipo1 = "";
        String pequipo2 = "";
        //////////////////primer equpo
        int pgolesenco = 0;
        int pgoleso = 0;
        int ppuntos = 0;
        //////////////// variables del rival
        int pgsolesenco = 0;//goles en contra
        int pgsoleso = 0;//goles
        int ppsuntos = 0;//puntos
        /////////////////////
        int ranpequipo1 = 0;
        int ranpequipo2 = 0;
        for (int x = 0; x < partido.size(); x++) {//saca los equipos de la lista
            pequipo1 = partido.get(0);
            pequipo2 = partido.get(1);
        }//recorre lista
        ranpequipo1 = random.nextInt(6);
        ranpequipo2 = random.nextInt(6);

        pgolesenco = ranpequipo2;//goles en contra del equipo 1
        pgsolesenco = ranpequipo1;//goles en contra del equipo 2

        pgoleso = ranpequipo1;//goles del equipo 1
        pgsoleso = ranpequipo2;//goles del equipo 2       

        int gana = 1;//para sumar con los partidos ganados
        String ganante = "";//saca los partidos ganados anteriormente
        int gatotal = 0;
        int gatotalconver = 0;

        if (ranpequipo1 > ranpequipo2) {//gana equipo 1 de la primera ronda
            equiposganados.add(pequipo1);
            partidosganados.add(0);
            ppuntos = 3;//equipo 1
            ppsuntos = 0;//equipo 2

            for (int x = 0; x < equiposganados.size(); x++) {
                if (pequipo1.equals(equiposganados.get(x))) {

                    gatotal = partidosganados.get(x);
                    gatotal = gatotal + gana;
                    partidosganados.set(x, gatotal);//corregir

                }//fin if

            }

        }
        if (ranpequipo2 > ranpequipo1) {
            equiposganados.add(pequipo2);
            partidosganados.add(0);

            ppuntos = 0;//equipo 1
            ppsuntos = 3;//equipo 2

            for (int x = 0; x < equiposganados.size(); x++) {
                if (pequipo2.equals(equiposganados.get(x))) {

                    gatotal = partidosganados.get(x);
                    gatotal = gatotal + gana;
                    partidosganados.set(x, gatotal);//corregir

                }//fin if

            }

        }//fin del if
        //System.out.println(equiposganados);
        //System.out.println(partidosganados);

        if (ranpequipo2 == ranpequipo1) {//desempate
            System.out.println("                                                          ");
            System.out.println("Desenpate entre:" + pequipo1 + " vs " + pequipo2);

            System.out.println("/Posicion de Lanzamiento/");

            System.out.println("//////");
            System.out.println("|3|2|1|\n"
                    + "|6|5|4|");
            System.out.println("//////");
            int atajar = 0;

            int validar = 0;
            int lanzamiento = 0;

            int goles = 0;
            int atajadas = 0;

            System.out.println("                          ");
            System.out.println("Lanza: " + pequipo1);
            System.out.println("Porterea: " + pequipo2);
            System.out.println("                          ");

            boolean vali = true;
            while (vali == true) {
                System.out.println("• arriba a la derecha digite(1):");
                System.out.println("• arriba al centro digite(2):");
                System.out.println("• arriba a la izquierda digite(3):");
                System.out.println("• abajo a la izquierda digite(4):");
                System.out.println("• abajo al centro digite(5):");
                System.out.println("• abajo a la derecha digite(6):");
                lanzamiento = numero.nextInt();
                atajar = random.nextInt(7);
                validar = validar + 1;

                if (lanzamiento == 1 && atajar == 0) {
                    atajadas = atajadas + 1;
                    System.out.println("Bola atajada.");

                }//fin if

                if (lanzamiento == 2 && atajar == 1) {
                    atajadas = atajadas + 1;
                    System.out.println("Bola atajada.");

                }//fin if

                if (lanzamiento == 3 && atajar == 2) {
                    atajadas = atajadas + 1;
                    System.out.println("Bola atajada.");

                }//fin if

                if (lanzamiento == 4 && atajar == 3) {
                    atajadas = atajadas + 1;
                    System.out.println("Bola atajada.");

                }//fin if

                if (lanzamiento == 5 && atajar == 4) {
                    atajadas = atajadas + 1;
                    System.out.println("Bola atajada.");

                }//fin if

                if (lanzamiento == 6 && atajar == 5) {
                    atajadas = atajadas + 1;
                    System.out.println("Bola atajada.");

                }//fin if 
                else {
                    goles = goles + 1;
                    System.out.println("GOOLLLLLLL.");
                }

                if (validar == 5) {
                    vali = false;
                    break;

                }

            }//fin del wuile
            System.out.println("                                                          ");

            if (goles > atajadas) {
                System.out.println("Gana: " + pequipo1);
                ppuntos = 1;//equipo 1
                ppsuntos = 0;//equipo 2
                
                         equiposganados.add(pequipo1);
            partidosganados.add(0);

            

            for (int x = 0; x < equiposganados.size(); x++) {
                if (pequipo1.equals(equiposganados.get(x))) {

                    gatotal = partidosganados.get(x);
                    gatotal = gatotal + gana;
                    partidosganados.set(x, gatotal);//corregir

                }//fin if

            }
                
                
                ///sumar 1 punto
            }
            if (atajadas > goles) {
                System.out.println("Gana: " + pequipo2);
                ppuntos = 0;//equipo 1
                ppsuntos = 1;//equipo 2
                
             equiposganados.add(pequipo2);
            partidosganados.add(0);

            

            for (int x = 0; x < equiposganados.size(); x++) {
                if (pequipo2.equals(equiposganados.get(x))) {

                    gatotal = partidosganados.get(x);
                    gatotal = gatotal + gana;
                    partidosganados.set(x, gatotal);//corregir

                }//fin if

            }
                
                

                ///sumar 1 punto
            }

            System.out.println("                                                          ");

        }//fin del if desempate 
        System.out.println("/////////////////////////////////////////////////////////////////////");
        System.out.println("Equipos= " + pequipo1 + " vs " + pequipo2);
        System.out.println("Marcador= " + pequipo1 + " Goles= " + pgoleso + " y " + pequipo2 + " Goles= " + pgsoleso);

        System.out.println("Puntos= " + pequipo1 + " Puntos= " + ppuntos + " y " + pequipo2 + " Puntos= " + ppsuntos);

        GregorianCalendar objFecha = new GregorianCalendar();

        int año = objFecha.get(Calendar.YEAR);
        int mes = objFecha.get(Calendar.MONTH) + 1;
        int dia = objFecha.get(Calendar.DAY_OF_MONTH);

        String pasaraño = String.valueOf(año);
        String pasarmes = String.valueOf(mes);
        String pasardia = String.valueOf(dia);

        String fecha = pasaraño + "/" + pasarmes + "/" + pasardia;

        System.out.println("fecha: " + fecha);

        System.out.println("/////////////////////////////////////////////////////////////////////");
        ////////////////////////////////////ganador 2////////////////////////////////////
        String sequipo1 = "";
        String sequipo2 = "";
        //////////////////primer equpo
        int sgolesenco = 0;
        int sgoleso = 0;
        int spuntos = 0;
        //////////////// variables del rival
        int sgsolesenco = 0;//goles en contra
        int sgsoleso = 0;//goles
        int spsuntos = 0;//puntos
        /////////////////////
        int sranpequipo1 = 0;//random
        int sranpequipo2 = 0;

        sranpequipo1 = random.nextInt(6);
        sranpequipo2 = random.nextInt(6);

        sgoleso = sranpequipo1;//goles del equipo 1
        sgolesenco = sranpequipo2;//goles en contra del equipo 1

        sgsolesenco = sranpequipo1;//goles del equipo 2
        sgsoleso = sranpequipo2;//goles en contra del equipo 2

        sequipo1 = partido.get(2);
        sequipo2 = partido.get(3);

        int sgana = 1;//para sumar con los partidos ganados

        int sgatotal = 0;

        if (sranpequipo1 > sranpequipo2) {//gana equipo 1 de la primera ronda
            equiposganados.add(sequipo1);
            partidosganados.add(0);
            spuntos = 3;
            spsuntos = 0;

            for (int x = 0; x < equiposganados.size(); x++) {
                if (sequipo1.equals(equiposganados.get(x))) {

                    sgatotal = partidosganados.get(x);
                    sgatotal = sgatotal + sgana;
                    partidosganados.set(x, sgatotal);//corregir

                }//fin if

            }//fin del ford

        }
        if (sranpequipo2 > sranpequipo1) {
            equiposganados.add(sequipo2);
            partidosganados.add(0);

            spuntos = 0;
            spsuntos = 3;

            for (int x = 0; x < equiposganados.size(); x++) {
                if (sequipo2.equals(equiposganados.get(x))) {

                    sgatotal = partidosganados.get(x);
                    sgatotal = sgatotal + sgana;
                    partidosganados.set(x, sgatotal);//corregir

                }//fin if
            }//fin del ford

        }//fin del if para sacar si gano el 2

        if (sranpequipo1 == sranpequipo2) {

            System.out.println("Desenpate entre:" + sequipo1 + " vs " + sequipo2);
            System.out.println("/Posicion de Lanzamiento/");
            System.out.println("//////");
            System.out.println("|3|2|1|\n"
                    + "|6|5|4|");
            System.out.println("//////");

            int atajar = 0;

            int validar = 0;
            int lanzamiento = 0;

            int goles = 0;
            int atajadas = 0;

            System.out.println("                          ");
            System.out.println("Lanza: " + sequipo1);
            System.out.println("Porterea: " + sequipo2);
            System.out.println("                          ");

            boolean vali = true;
            while (vali == true) {
                System.out.println("• arriba a la derecha digite(1):");
                System.out.println("• arriba al centro digite(2):");
                System.out.println("• arriba a la izquierda digite(3):");
                System.out.println("• abajo a la izquierda digite(4):");
                System.out.println("• abajo al centro digite(5):");
                System.out.println("• abajo a la derecha digite(6):");
                lanzamiento = numero.nextInt();
                atajar = random.nextInt(7);
                validar = validar + 1;

                if (lanzamiento == 1 && atajar == 0) {
                    atajadas = atajadas + 1;
                    System.out.println("Bola atajada.");

                }//fin if

                if (lanzamiento == 2 && atajar == 1) {
                    atajadas = atajadas + 1;
                    System.out.println("Bola atajada.");

                }//fin if

                if (lanzamiento == 3 && atajar == 2) {
                    atajadas = atajadas + 1;
                    System.out.println("Bola atajada.");

                }//fin if

                if (lanzamiento == 4 && atajar == 3) {
                    atajadas = atajadas + 1;
                    System.out.println("Bola atajada.");

                }//fin if

                if (lanzamiento == 5 && atajar == 4) {
                    atajadas = atajadas + 1;
                    System.out.println("Bola atajada.");

                }//fin if

                if (lanzamiento == 6 && atajar == 5) {
                    atajadas = atajadas + 1;
                    System.out.println("Bola atajada.");

                }//fin if 
                else {
                    goles = goles + 1;
                    System.out.println("GOOLLLLLLL.");
                }

                if (validar == 5) {
                    vali = false;
                    break;

                }

            }//fin del wuile
            System.out.println("                                                          ");

            if (goles > atajadas) {
                System.out.println("Gana: " + sequipo1);

                spuntos = 1;
                spsuntos = 0;

                equiposganados.add(sequipo1);
            partidosganados.add(0);

           

            for (int x = 0; x < equiposganados.size(); x++) {
                if (sequipo1.equals(equiposganados.get(x))) {

                    sgatotal = partidosganados.get(x);
                    sgatotal = sgatotal + sgana;
                    partidosganados.set(x, sgatotal);//corregir

                }//fin if
            }//fin del ford
                
                
                
                
                ///sumar 1 punto
            }
            if (atajadas > goles) {
                System.out.println("Gana: " + sequipo2);
                spuntos = 0;
                spsuntos = 1;
                
              equiposganados.add(sequipo2);
            partidosganados.add(0);

           

            for (int x = 0; x < equiposganados.size(); x++) {
                if (sequipo2.equals(equiposganados.get(x))) {

                    sgatotal = partidosganados.get(x);
                    sgatotal = sgatotal + sgana;
                    partidosganados.set(x, sgatotal);//corregir

                }//fin if
            }//fin del ford
                
                

                ///sumar 1 punto
            }

            System.out.println("                                                          ");

        } //fin del desempate 2

        System.out.println("/////////////////////////////////////////////////////////////////////");
        System.out.println("Equipos= " + sequipo1 + " vs " + sequipo2);
        System.out.println("Marcador= " + sequipo1 + " Goles= " + sgoleso + " y " + sequipo2 + " Goles= " + sgsoleso);

        System.out.println("Puntos= " + sequipo1 + " Puntos= " + spuntos + " y " + sequipo2 + " Puntos= " + spsuntos);
        System.out.println("fecha: " + fecha);

        System.out.println("/////////////////////////////////////////////////////////////////////");

        ///////////////////////////////////ganador 3////////////////////////////////////      
        String tequipo1 = "";
        String tequipo2 = "";
        //////////////////primer equpo
        int tgolesenco = 0;
        int tgoleso = 0;
        int tpuntos = 0;
        //////////////// variables del rival
        int tgsolesenco = 0;//goles en contra
        int tgsoleso = 0;//goles
        int tpsuntos = 0;//puntos
        /////////////////////
        int tranpequipo1 = 0;//random
        int tranpequipo2 = 0;

        tranpequipo1 = random.nextInt(6);
        tranpequipo2 = random.nextInt(6);

        tgolesenco = tranpequipo2;//goles en contra del equipo 1
        tgoleso = tranpequipo1;//goles del equipo 1

        tgsoleso = tranpequipo2;//goles del equipo 2
        tgsolesenco = tranpequipo1;//goles en contra del equipo 2 

        tequipo1 = partido.get(4);
        tequipo2 = partido.get(5);

        int tgana = 1;//para sumar con los partidos ganados

        int tgatotal = 0;

        if (tranpequipo1 > tranpequipo2) {//gana equipo 1 de la primera ronda
            equiposganados.add(tequipo1);
            partidosganados.add(0);
            tpuntos = 3;//puntos del equipo1
            tpsuntos = 0;//puntos del equipo2

            for (int x = 0; x < equiposganados.size(); x++) {
                if (tequipo1.equals(equiposganados.get(x))) {

                    tgatotal = partidosganados.get(x);
                    tgatotal = tgatotal + tgana;
                    partidosganados.set(x, tgatotal);//corregir

                }//fin if

            }//fin del ford

        }

        if (tranpequipo2 > tranpequipo1) {
            equiposganados.add(tequipo2);
            partidosganados.add(0);

            tpuntos = 0;//puntos del equipo1
            tpsuntos = 3;//puntos del equipo2

            for (int x = 0; x < equiposganados.size(); x++) {
                if (tequipo2.equals(equiposganados.get(x))) {

                    tgatotal = partidosganados.get(x);
                    tgatotal = tgatotal + tgana;
                    partidosganados.set(x, tgatotal);//corregir

                }//fin if
            }//fin del ford

        }//fin del if para sacar si gano el 2

        if (tranpequipo1 == tranpequipo2) {
            System.out.println("Desenpate entre:" + tequipo1 + " vs " + tequipo2);

            System.out.println("/Posicion de Lanzamiento/");
            System.out.println("//////");
            System.out.println("|3|2|1|\n"
                    + "|6|5|4|");
            System.out.println("//////");

            int atajar = 0;

            int validar = 0;
            int lanzamiento = 0;

            int goles = 0;
            int atajadas = 0;

            System.out.println("                          ");
            System.out.println("Lanza: " + tequipo1);
            System.out.println("Porterea: " + tequipo2);
            System.out.println("                          ");

            boolean vali = true;
            while (vali == true) {
                System.out.println("• arriba a la derecha digite(1):");
                System.out.println("• arriba al centro digite(2):");
                System.out.println("• arriba a la izquierda digite(3):");
                System.out.println("• abajo a la izquierda digite(4):");
                System.out.println("• abajo al centro digite(5):");
                System.out.println("• abajo a la derecha digite(6):");
                lanzamiento = numero.nextInt();
                atajar = random.nextInt(7);
                validar = validar + 1;

                if (lanzamiento == 1 && atajar == 0) {
                    atajadas = atajadas + 1;
                    System.out.println("Bola atajada.");

                }//fin if

                if (lanzamiento == 2 && atajar == 1) {
                    atajadas = atajadas + 1;
                    System.out.println("Bola atajada.");

                }//fin if

                if (lanzamiento == 3 && atajar == 2) {
                    atajadas = atajadas + 1;
                    System.out.println("Bola atajada.");

                }//fin if

                if (lanzamiento == 4 && atajar == 3) {
                    atajadas = atajadas + 1;
                    System.out.println("Bola atajada.");

                }//fin if

                if (lanzamiento == 5 && atajar == 4) {
                    atajadas = atajadas + 1;
                    System.out.println("Bola atajada.");

                }//fin if

                if (lanzamiento == 6 && atajar == 5) {
                    atajadas = atajadas + 1;
                    System.out.println("Bola atajada.");

                }//fin if 
                else {
                    goles = goles + 1;
                    System.out.println("GOOLLLLLLL.");
                }

                if (validar == 5) {
                    vali = false;
                    break;

                }

            }//fin del wuile
            System.out.println("                                                          ");

            if (goles > atajadas) {
                System.out.println("Gana: " + tequipo1);
                tpuntos = 1;//puntos del equipo1
                tpsuntos = 0;//puntos del equipo2
                
             equiposganados.add(tequipo1);
            partidosganados.add(0);
           

            for (int x = 0; x < equiposganados.size(); x++) {
                if (tequipo1.equals(equiposganados.get(x))) {

                    tgatotal = partidosganados.get(x);
                    tgatotal = tgatotal + tgana;
                    partidosganados.set(x, tgatotal);//corregir

                }//fin if

            }//fin del ford
                
                

                ///sumar 1 punto
            }
            if (atajadas > goles) {
                System.out.println("Gana: " + tequipo2);
                tpuntos = 0;//puntos del equipo1
                tpsuntos = 1;//puntos del equipo2
                ///sumar 1 punto
                     equiposganados.add(tequipo2);
            partidosganados.add(0);
            

            for (int x = 0; x < equiposganados.size(); x++) {
                if (tequipo2.equals(equiposganados.get(x))) {

                    tgatotal = partidosganados.get(x);
                    tgatotal = tgatotal + tgana;
                    partidosganados.set(x, tgatotal);//corregir

                }//fin if

            }//fin del ford
                
                
                
            }

            System.out.println("                                                          ");

        }

        System.out.println("/////////////////////////////////////////////////////////////////////");
        System.out.println("Equipos= " + tequipo1 + " vs " + tequipo2);
        System.out.println("Marcador= " + tequipo1 + " Goles= " + tgoleso + " y " + tequipo2 + " Goles= " + tgsoleso);

        System.out.println("Puntos= " + tequipo1 + " Puntos= " + tpuntos + " y " + tequipo2 + " Puntos= " + tpsuntos);
        System.out.println("fecha: " + fecha);

        System.out.println("/////////////////////////////////////////////////////////////////////");

        ///////////////////SECCION DE AGREGAR DATOS Y SUMAR GOLES ,GOLES EN CONTRA Y PUNTOS PARA AGREGAR A LA MATRIZ//////////////
        for (int x = 0; x < matriz.length; x++) {//meter el equipo 1 a la matriz
            for (int y = 0; y < matriz[x].length; y++) {
                if (matriz[x][0].equals(pequipo1)) {
                    ///mete goles
                    int nuevovalor = 0;
                    String valordematriz = "";
                    valordematriz = matriz[x][1];
                    nuevovalor = Integer.parseInt(valordematriz);

                    pgoleso = pgoleso + nuevovalor;

                    String golesequipo1p = pgoleso + "";
                    matriz[x][1] = golesequipo1p;
                    //////////////////////////sigue meter goles en contra
                    int nuevovalorgc = 0;
                    String valordematrizgc = "";
                    valordematrizgc = matriz[x][2];
                    nuevovalorgc = Integer.parseInt(valordematrizgc);

                    pgolesenco = pgolesenco + nuevovalorgc;

                    String golesequipo1pgc = pgolesenco + "";
                    matriz[x][2] = golesequipo1pgc;
                    ////////////////////////sigue meter los puntos
                    int nuevovalorpt = 0;
                    String valordematrizpt = "";
                    valordematrizpt = matriz[x][3];
                    nuevovalorpt = Integer.parseInt(valordematrizpt);

                    ppuntos = ppuntos + nuevovalorpt;

                    String golesequipo1pt = ppuntos + "";
                    matriz[x][3] = golesequipo1pt;

                    break;
                    /////////////////saca todos los goles

                }//fin del if 1

                if (matriz[x][0].equals(pequipo2)) {//equipo 2
                    ///mete goles
                    int nuevovalor = 0;
                    String valordematriz = "";
                    valordematriz = matriz[x][1];
                    nuevovalor = Integer.parseInt(valordematriz);

                    pgsoleso = pgsoleso + nuevovalor;

                    String golesequipo1p = pgsoleso + "";
                    matriz[x][1] = golesequipo1p;
                    //////////////////////////sigue meter goles en contra
                    int nuevovalorgc = 0;
                    String valordematrizgc = "";
                    valordematrizgc = matriz[x][2];
                    nuevovalorgc = Integer.parseInt(valordematrizgc);

                    pgsolesenco = pgsolesenco + nuevovalorgc;

                    String golesequipo1pgc = pgsolesenco + "";
                    matriz[x][2] = golesequipo1pgc;
                    ////////////////////////sigue meter los puntos
                    int nuevovalorpt = 0;
                    String valordematrizpt = "";
                    valordematrizpt = matriz[x][3];
                    nuevovalorpt = Integer.parseInt(valordematrizpt);

                    ppsuntos = ppsuntos + nuevovalorpt;

                    String golesequipo1pt = ppsuntos + "";
                    matriz[x][3] = golesequipo1pt;

                    break;
                    /////////////////saca todos los goles

                }//fin del if 2

                if (matriz[x][0].equals(sequipo1)) {//equipo 3

                    ///mete goles
                    int nuevovalor = 0;
                    String valordematriz = "";
                    valordematriz = matriz[x][1];
                    nuevovalor = Integer.parseInt(valordematriz);

                    sgoleso = sgoleso + nuevovalor;

                    String golesequipo1p = sgoleso + "";
                    matriz[x][1] = golesequipo1p;
                    //////////////////////////sigue meter goles en contra
                    int nuevovalorgc = 0;
                    String valordematrizgc = "";
                    valordematrizgc = matriz[x][2];
                    nuevovalorgc = Integer.parseInt(valordematrizgc);

                    sgolesenco = sgolesenco + nuevovalorgc;

                    String golesequipo1pgc = sgolesenco + "";
                    matriz[x][2] = golesequipo1pgc;
                    ////////////////////////sigue meter los puntos
                    int nuevovalorpt = 0;
                    String valordematrizpt = "";
                    valordematrizpt = matriz[x][3];
                    nuevovalorpt = Integer.parseInt(valordematrizpt);

                    spuntos = spuntos + nuevovalorpt;

                    String golesequipo1pt = spuntos + "";
                    matriz[x][3] = golesequipo1pt;

                    break;
                    /////////////////saca todos los goles

                }//fin del if 3

                if (matriz[x][0].equals(sequipo2)) {//equipo 4

                    ///mete goles
                    int nuevovalor = 0;
                    String valordematriz = "";
                    valordematriz = matriz[x][1];
                    nuevovalor = Integer.parseInt(valordematriz);

                    sgsoleso = sgsoleso + nuevovalor;

                    String golesequipo1p = sgsoleso + "";
                    matriz[x][1] = golesequipo1p;
                    //////////////////////////sigue meter goles en contra
                    int nuevovalorgc = 0;
                    String valordematrizgc = "";
                    valordematrizgc = matriz[x][2];
                    nuevovalorgc = Integer.parseInt(valordematrizgc);

                    sgsolesenco = sgsolesenco + nuevovalorgc;

                    String golesequipo1pgc = sgsolesenco + "";
                    matriz[x][2] = golesequipo1pgc;
                    ////////////////////////sigue meter los puntos
                    int nuevovalorpt = 0;
                    String valordematrizpt = "";
                    valordematrizpt = matriz[x][3];
                    nuevovalorpt = Integer.parseInt(valordematrizpt);

                    spsuntos = spsuntos + nuevovalorpt;

                    String golesequipo1pt = spsuntos + "";
                    matriz[x][3] = golesequipo1pt;

                    break;
                    /////////////////saca todos los goles

                }//fin del if 4

                if (matriz[x][0].equals(tequipo1)) {//equipo 5

                    ///mete goles
                    int nuevovalor = 0;
                    String valordematriz = "";
                    valordematriz = matriz[x][1];
                    nuevovalor = Integer.parseInt(valordematriz);

                    tgoleso = tgoleso + nuevovalor;

                    String golesequipo1p = tgoleso + "";
                    matriz[x][1] = golesequipo1p;
                    //////////////////////////sigue meter goles en contra
                    int nuevovalorgc = 0;
                    String valordematrizgc = "";
                    valordematrizgc = matriz[x][2];
                    nuevovalorgc = Integer.parseInt(valordematrizgc);

                    tgolesenco = tgolesenco + nuevovalorgc;

                    String golesequipo1pgc = tgolesenco + "";
                    matriz[x][2] = golesequipo1pgc;
                    ////////////////////////sigue meter los puntos
                    int nuevovalorpt = 0;
                    String valordematrizpt = "";
                    valordematrizpt = matriz[x][3];
                    nuevovalorpt = Integer.parseInt(valordematrizpt);

                    tpuntos = tpuntos + nuevovalorpt;

                    String golesequipo1pt = tpuntos + "";
                    matriz[x][3] = golesequipo1pt;

                    break;
                    /////////////////saca todos los goles

                }//fin del if 5

                if (matriz[x][0].equals(tequipo2)) {//equipo 6

                    ///mete goles
                    int nuevovalor = 0;
                    String valordematriz = "";
                    valordematriz = matriz[x][1];
                    nuevovalor = Integer.parseInt(valordematriz);

                    tgsoleso = tgsoleso + nuevovalor;

                    String golesequipo1p = tgsoleso + "";
                    matriz[x][1] = golesequipo1p;
                    //////////////////////////sigue meter goles en contra
                    int nuevovalorgc = 0;
                    String valordematrizgc = "";
                    valordematrizgc = matriz[x][2];
                    nuevovalorgc = Integer.parseInt(valordematrizgc);

                    tgsolesenco = tgsolesenco + nuevovalorgc;

                    String golesequipo1pgc = tgsolesenco + "";
                    matriz[x][2] = golesequipo1pgc;
                    ////////////////////////sigue meter los puntos
                    int nuevovalorpt = 0;
                    String valordematrizpt = "";
                    valordematrizpt = matriz[x][3];
                    nuevovalorpt = Integer.parseInt(valordematrizpt);

                    tpsuntos = tpsuntos + nuevovalorpt;

                    String golesequipo1pt = tpsuntos + "";
                    matriz[x][3] = golesequipo1pt;

                    break;
                    /////////////////saca todos los goles

                }//fin del if 6

            }

        }

    }//fin del partido

    public static void tabla() {
///no se logro mover campos en la matriz,se intento hacer en un proyecto aparte pero la funcion no dio un buen desempeño
        String mostrar = "";
        for (int x = 0; x < matriz.length; x++) {
            for (int y = 0; y < matriz[x].length; y++) {

                mostrar = mostrar + "|" + matriz[x][y];
            }//fin del ford columna
            mostrar = mostrar + "\n";

        }//fin del ford fila
        System.out.println(mostrar);

    }//fin de la funcion tabla

    
 public static void reporte(){
//esta lista tiene los equipos///equiposganados
System.out.println("/////////////reporte/////////////");

    String lda="";
    int ldago=0;
    boolean va1=false;
   for (int x = 0; x < equiposganados.size(); x++) {
      
       if("LDA        ".equals(equiposganados.get(x))){
           lda=equiposganados.get(x);
           ldago=partidosganados.get(x);
           va1=true;
       }
      
          
           
   }  
   if(va1==true){
   System.out.println(lda+ "= "+ldago+" partidos ganados.");
   }
   
   String ldas="";
    int ldagos=0;
    boolean va2=false;
   
      for (int x = 0; x < equiposganados.size(); x++) {
          if("SAPRISA    ".equals(equiposganados.get(x))){
              va2=true;
              ldas=  equiposganados.get(x); 
              ldagos=partidosganados.get(x);
              break;
       }
          
      } 
      if(va2==true){
      System.out.println(ldas+ "= "+ldagos+" partidos ganados.");
      }
      
      String ldat="";
    int ldagot=0;
    boolean va3=false;
      for (int x = 0; x < equiposganados.size(); x++) {    
          if("SAN CARLOS ".equals(equiposganados.get(x))){
              ldat=equiposganados.get(x);
              ldagot=partidosganados.get(x);
              va3=true;
           break;
                               
       }
          
      }
      if(va3==true){
       System.out.println(ldat+ "= "+ldagot+" partidos ganados.");
      }
      
       boolean va4=false;
      String ldac="";
    int ldagoc=0;
      for (int x = 0; x < equiposganados.size(); x++) {
          if("HEREDIA    ".equals(equiposganados.get(x))){
              ldac=equiposganados.get(x);
              ldagoc=partidosganados.get(x);
              va4=true;
                  break;              
       }
           
      }
      if(va4==true){
      System.out.println(ldac+ "= "+ldagoc+" partidos ganados.");
      }
      String ldaq="";
    int ldagoq=0;
    boolean va5=false;
      for (int x = 0; x < equiposganados.size(); x++) {
           if("CARTAGINES ".equals(equiposganados.get(x))){
               ldaq=equiposganados.get(x);
               ldagoq=partidosganados.get(x);
               va5=true;
           break;
                                
       }
          
      }
      if(va5==true){
      System.out.println(ldaq+ "= "+ldagoq+" partidos ganados.");
      }
      
      String ldase="";
    int ldagose=0;
    boolean va6=false;
      for (int x = 0; x < equiposganados.size(); x++) {
          if("PUNTARENAS ".equals(equiposganados.get(x))){
              ldase=equiposganados.get(x);
              ldagose=partidosganados.get(x);
              va6=true;
            break;
          }
          
      }
      if(va6==true){
     System.out.println(ldase+ "= "+ldagose+" partidos ganados.");
      }
      
}//fin de la funcion reporte   
    
    
    
}//fin funcion

